import Control.Monad (forM_)
import System.IO (hFlush, stdout)
import Text.Read (readMaybe)

main :: IO ()
main = do
    putStr "saisie 1 : "
    entier1 <- getLine
    let n <- (read entier1 ::String)
    putStr "Vous avez saisi l'entier "
    putStrLn show n

