
fizzbuzz1 :: [Int] -> [String]
fizzbuzz1 (h:t)
    | h `mod` 3 == 0 && h `mod` 15 == 0 = "fizzbuzz" : fizzbuzz1 t
    | h `mod` 3 == 0 = "fizz" : fizzbuzz1 t
    | h `mod` 5 == 0 = "buzz" : fizzbuzz1 t
    | otherwise = show h : fizzbuzz1 t

-- fizzbuzz2 :: [Int] -> [String]

-- fizzbuzz

main :: IO ()
main = putStrLn "TODO"

