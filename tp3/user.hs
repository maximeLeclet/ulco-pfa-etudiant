
-- TODO User
data User = User
    { _nom    :: String
    , _prenom :: String
    , _age    :: Int
    } 

-- showUser :: User -> String
showUser (User nom prenom age) = nom ++ " " ++ prenom ++ " (" ++ show age ++ " ans)"

incAge :: User -> User
incAge user = user { _age = _age user + 1 }

main :: IO ()
main = putStrLn "TODO"

