
-- TODO List
data List
    = Nil
    | Cons Int List
    deriving Show

sumList :: Num a => List a -> a
sumList Nil = 0
sumList (Cons x xs) = x + sumList xs 

-- flatList 

-- toHaskell 
toHaskell :: List a -> [a]
toHaskell Nil = []
toHaskell (Cons h t) = h:toHaskell t 

-- fromHaskell
-- fromHaskell 
fromHaskell :: [a] -> List a
fromHaskell [] = Nil
fromHaskell (x:xs) = Cons x (fromHaskell xs)

-- myShowList
--myShowList :: Show a => List a -> String
myShowList :: Show a => List a -> String
myShowList Nil = ""
myShowList (Cons x xs) = show x ++ " " ++ myShowList xs

main :: IO ()
main = putStrLn "TODO"

