
-- TODO Jour
data Jour = Lundi | Mardi | Mercredi | Jeudi | Vendredi | Samedi | Dimanche

estWeekend :: Jour -> Bool
estWeekend Samedi = True
estWeekend Dimanche = True
estWeekend _ = False

compterOuvrables :: [Jour] -> Int
compterOuvrables [] = 0
compterOuvrables (h:t)
    | estWeekend h == True = compterOuvrables t
    | otherwise = 1 + compterOuvrables t

main :: IO ()
main = putStrLn "TODO"


