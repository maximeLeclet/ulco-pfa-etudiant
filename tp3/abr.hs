import Data.List (foldl')

-- TODO Abr
data Abr = Empty | Tree Int Abr Abr

insererAbr :: Abr -> Int -> Abr
insererAbr Empty x = Tree x Empty Empty
insererAbr (Tree r Empty Empty) x
    | x <= r = (Tree r (Tree x Empty Empty) Empty)
    | otherwise = (Tree x Empty (Tree r Empty Empty))
insererAbr (Tree r lT rT) x
    | x <= r = insererAbr lT x
    | otherwise = insererAbr rT x

-- listToAbr 

-- abrToList 

main :: IO ()
main = putStrLn "TODO"

